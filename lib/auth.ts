import { IAccount, ICreateAccount } from "./accounts";

export interface ISignIn extends Pick<IAccount, "email" | "password"> {}

export type ISignUp = ICreateAccount;

export type IRecoveryPassword = Pick<IAccount, "email">;

export interface ITokens {
	accessToken: string;
	refreshToken: string;
}

export type IJWTTokenData = {
	sub: number;
	iat: number;
	exp: number;
	aud: string;
	iss: string;
};
