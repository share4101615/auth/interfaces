export interface IAccount {
	id: number;
	email: string;
	password: string;
	createdAt: Date;
	updatedAt: Date;
	firstName: string | null;
	lastName: string | null;
}

export interface IGetAccount extends Omit<IAccount, "password"> {}

export interface ICreateAccount extends Pick<IAccount, "email" | "password">, Partial<Pick<IAccount, "firstName" | "lastName">> {}

export type IUpdateAccount = Partial<ICreateAccount>;
